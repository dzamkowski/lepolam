<div class="menu-box">
	<a href="/"><img class="logo" src="<?php echo get_bloginfo('template_url') ?>/images/logo.svg" alt="Lepolam"></a>
	<nav class="menu">
		<a href="/" class="hidden-menu-item"></a>
		<?php header_nav(); ?>
	</nav>
	<div class="social text-right">
		<a class="facebook" href="#" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a>
		<a href="#" target="_blank"><i class="fa fa-youtube" aria-hidden="true"></i></a>
	</div>
</div>

<div class="mobile-menu-box">
	<a href="/"><img class="logo" src="<?php echo get_bloginfo('template_url') ?>/images/logo.svg" alt="Lepolam"></a>
	<h2>Menu</h2>
	<div class="mobile-menu-icon">
		<span></span>
		<span></span>
		<span></span>
		<span></span>
	</div>
	
	<nav class="menu mobile-menu">
		<?php header_nav(); ?>
		<div class="social">
			<a class="facebook" href="#" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a>
			<a href="#" target="_blank"><i class="fa fa-youtube" aria-hidden="true"></i></a>
		</div>
	</nav>
</div>