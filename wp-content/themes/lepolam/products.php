<div class="products" id="products">
	<div class="container">
		<div class="row">
		    <h2 class="title col-md-12 text-center">Nasze produkty</h2>
				<?php 
					$page_taxi = new WP_Query( 'page_id=4' ); 
					$page_ads = new WP_Query( 'page_id=177' ); 
					$page_esiva = new WP_Query( 'page_id=179' ); 
				?>

				<?php if ($page_taxi -> have_posts()) : $page_taxi -> the_post();  ?>
	                <a class="main-product col-xs-4 col-md-4" href="<?php the_permalink(); ?>">
	                    <img class="product-img" src="<?php echo wp_get_attachment_url( get_post_thumbnail_id() );?>" alt="">
	                    <p class="product-name text-center"><?php the_title(); ?></p>
	                </a>
                <?php endif;?>

                <?php if ($page_ads -> have_posts()) : $page_ads -> the_post();  ?>
	                <a class="main-product col-xs-4 col-md-4" href="<?php the_permalink(); ?>">
	                    <img class="product-img" src="<?php echo wp_get_attachment_url( get_post_thumbnail_id() );?>" alt="">
	                    <p class="product-name text-center"><?php the_title(); ?></p>
	                </a>
                <?php endif;?>

                <?php if ($page_esiva -> have_posts()) : $page_esiva -> the_post();  ?>
	                <a class="main-product col-xs-4 col-md-4" href="<?php the_permalink(); ?>">
	                    <img class="product-img" src="<?php echo wp_get_attachment_url( get_post_thumbnail_id() );?>" alt="">
	                    <p class="product-name text-center"><?php the_title(); ?></p>
	                </a>
                <?php endif;?>
		</div>
	</div>
</div>

