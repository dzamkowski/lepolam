<header class="header page-header">
	<div class="container">
		<div class="row">
			<?php get_template_part('menu'); ?>
		</div>
	</div>
	<div class="banner-page">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<div class="row">
						<h1 class="title-page">
							<?php 
							$text = get_field('banner_title');
							if( $text ) { ?>

							<?php echo get_field('banner_title'); ?>

							<?php } else {
								wp_title(''); 
								}
							?>
						</h1>
						<div class="text-page"><?php echo get_field('banner_text'); ?></div>
						<img class="cert" src="<?php echo get_bloginfo('template_url') ?>/images/cert-logo.png" alt="Certyfikat Produkt Polski">
					</div>
				</div>
			</div>
		</div>
	</div>
</header>