<?php /* Template Name: Archives */ ?>

<!DOCTYPE html>
<html>
	<head>
	    <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <link href="<?php echo get_template_directory_uri(); ?>/images/favicon.ico" rel="shortcut icon" />
		<title><?php wp_title(); ?></title>
		<?php wp_head(); ?>
	</head>
	<body>
	<header class="header page-header">
	<div class="container">
		<div class="row">
			<?php get_template_part('menu'); ?>
		</div>
		</div>
			<div class="banner-page">
				<div class="container">
					<div class="row">
						<div class="col-xs-12">
							<div class="row">
								<h1 class="title-page">
									<?php 
										$text = get_field('banner_title');
										if( $text ) { ?>

										<?php echo get_field('banner_title'); ?>

										<?php } else {
											wp_title(''); 
											}
									?>
								</h1>
								<img class="cert" src="<?php echo get_bloginfo('template_url') ?>/images/cert-logo.png" alt="Certyfikat Produkt Polski">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>
		<main>
			<div class="single archive container">
				<div class="row">
					<?php 
					$args = array(
						'posts_per_page' => -1,
						'offset'           => 0,
						'category'         => '4',
						'orderby'          => 'date',
						'order'            => 'DESC',
						'post_type'        => 'post',
						'post_status'      => 'publish',
						'suppress_filters' => true 
					);
					$posts = get_posts( $args ); 
					?>
					<?php foreach ( $posts as $post ) : setup_postdata( $post ); ?>
						<div class="main-post col-xs-12 col-md-4">
							<div class="row">
								<a class="post-title" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
								<p class="post-date"><?php the_time('j.m.Y'); ?></p>
								<div class="post-lead"><?php the_excerpt(); ?></div>
								<a class="post-read-more" href="<?php the_permalink(); ?>" title="Czytaj więcej">Czytaj więcej</a>
							</div>
						</div>
					<?php endforeach; 
					wp_reset_postdata();
					?>
				</div>
			</div>
			<?php get_template_part('realizations'); ?>
		</main>
	<?php get_footer(); ?>
	</body>
</html>