<div class="aside">
	<div class="container">
		<div class="row">
			<?php while( have_rows('texts_home', 'option') ): the_row(); ?>
				<div class="col-md-6">
					<div class="row">
						<div class="text_home_1"><?php the_sub_field('text_home_1'); ?></div>
						<div class="text_home_2"><?php the_sub_field('text_home_2'); ?></div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="row">
						<div class="text_home_3"><?php the_sub_field('text_home_3'); ?></div>
						<div class="text_home_4"><?php the_sub_field('text_home_4'); ?></div>
					</div>
				</div>
			<?php endwhile; ?>
		</div>
	</div>
</div>