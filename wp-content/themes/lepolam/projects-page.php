<?php /* Template Name: Projects */ ?>

<!DOCTYPE html>
<html>
	<head>
	    <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <link href="<?php echo get_template_directory_uri(); ?>/images/favicon.ico" rel="shortcut icon" />
		<title><?php wp_title(); ?></title>
		<?php wp_head(); ?>
	</head>
	<body>
	<?php get_template_part('page-header'); ?>
		<main>
			<div class="products-page container">
				<?php 

				$image_ids = get_field('gallery_projects', false, false);
				$shortcode = '[' . 'gallery ids="' . implode(',', $image_ids) . '"]';

				echo do_shortcode( $shortcode );

				?>
			</div>
			<?php get_template_part('realizations'); ?>
		</main>
	<?php get_footer(); ?>
	</body>
</html>