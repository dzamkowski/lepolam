<div class="news">
	<div class="container">
		<div class="row">
			<h2 class="title col-md-12 text-center">Aktualności</h2>
			<?php 
				$args = array(
					'posts_per_page'   => 3,
					'offset'           => 0,
					'category'         => '4',
					'orderby'          => 'date',
					'order'            => 'DESC',
					'post_type'        => 'post',
					'post_status'      => 'publish',
					'suppress_filters' => true 
				);
				$posts = get_posts( $args ); 
			?>
			<?php foreach ( $posts as $post ) : setup_postdata( $post ); ?>
				<div class="main-post col-xs-12 col-md-4">
					<div class="row">
						<a class="post-title" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
						<p class="post-date"><?php the_time('j.m.Y'); ?></p>
						<div class="post-lead"><?php the_excerpt(); ?></div>
						<a class="post-read-more" href="<?php the_permalink(); ?>" title="Czytaj więcej">Czytaj więcej</a>
					</div>
				</div>
			<?php endforeach; 
			wp_reset_postdata();
			?>
			<div class="read-more col-md-12 text-center"><a href="/archiwum" title="Zobacz wszystkie posty">Zobacz wszystkie wpisy</a><i class="fa fa-angle-down" aria-hidden="true"></i></div>
		</div>
	</div>
</div>

