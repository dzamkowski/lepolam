<header class="header">
	<div class="container">
		<div class="row">
			<?php get_template_part('menu'); ?>
			<div class="col-xs-12 col-md-6 col-md-push-6 taxi visible">
				<div class="row">
					<div id="banner_slider" class="owl-carousel">
						<?php while( have_rows('banner_img', 'option') ): the_row(); ?>
						<img class="taxi-img" src="<?php the_sub_field('image'); ?>" alt="">
						<?php endwhile; ?>
					</div>
					<img class="cert" src="<?php echo get_bloginfo('template_url') ?>/images/cert-logo.png" alt="Certyfikat Produkt Polski">
				</div>
			</div>

			<div class="col-xs-12 col-md-6 col-md-pull-6 visible">
				<div class="row">
					<h2 class="header-title">Produkcja<br> na <span class="orange">najwyższym</span> poziomie</h2>

					<?php while( have_rows('banner_home', 'option') ): the_row(); ?>
					<p class="header-text"><?php the_sub_field('paragraph_home'); ?></p>
					<?php endwhile; ?>

					<ul class="list">
						<?php while( have_rows('list_home', 'option') ): the_row(); ?>
						<li class="list-item">
							<img src="<?php the_sub_field('image'); ?>" alt="">
							<a href="<?php the_sub_field('link'); ?>"><?php the_sub_field('text'); ?></a>
						</li>
						<?php endwhile; ?>
						<li class="list-item">
							<img src="http://lepolam.test.designum.pl/wp-content/uploads/2018/03/air-ico.jpg" alt="">
							<a href="http://esiva.pl/" target="_blank">Produkty Esiva</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<img class="cert desktop-cert" src="<?php echo get_bloginfo('template_url') ?>/images/cert-logo.png" alt="Certyfikat Produkt Polski">
	</div>
</header>