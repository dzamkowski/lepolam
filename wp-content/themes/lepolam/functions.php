<?php

	// LOAD STYLES AND SCRIPTS
	function load_styles_and_scripts() {

		wp_enqueue_style(
			'bootstrap_css',
			'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css' 
		);

		wp_enqueue_style (
			'fancybox-styles',
			get_template_directory_uri() . '/plugins/fancybox/source/jquery.fancybox.css?v=2.1.5'
		);

		wp_enqueue_style (
			'owl-carousel-min-styles',
			get_template_directory_uri() . '/plugins/owlcarousel/owl.carousel.min.css'
		);

		wp_enqueue_style (
			'owl-carousel-styles',
			get_template_directory_uri() . '/plugins/owlcarousel/owl.theme.default.min.css'
		);

		wp_enqueue_style(
			'main_css', 
			get_template_directory_uri() . '/style.css' 
		);

		wp_enqueue_script (
			'jquery',
			'https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js'
		);

		wp_enqueue_script(
			'fancybox_script',
			get_template_directory_uri() . '/plugins/fancybox/source/jquery.fancybox.pack.js?v=2.1.5'
		);

		wp_enqueue_script(
			'bootstrap_js',
			'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js'
		);

		wp_enqueue_script (
			'owl-carousel-min-script',
			get_template_directory_uri() . '/plugins/owlcarousel/owl.carousel.min.js'
		);

		wp_enqueue_script (
			'font-awesome',
			'https://use.fontawesome.com/ef52789804.js'
		);

		wp_enqueue_script (
			'google-maps-script',
			'https://maps.googleapis.com/maps/api/js?key=AIzaSyBPM2D8gAdgaN-BiL0zbeFGiRy0vS_8vOE&callback=initMap',
			array(),
	        null,
	        null
		);

		wp_enqueue_script(
			'my_custom_js',
			get_template_directory_uri() . '/scripts/script.js'
		);

	}

	add_action( 'wp_enqueue_scripts', 'load_styles_and_scripts');


	// MENU
	register_nav_menus();

	function wpb_custom_new_menu() {
		register_nav_menu('top-menu',__( 'Top menu' ));
		register_nav_menu('coop-menu',__( 'Coop menu' ));
		register_nav_menu('bottom-menu',__( 'Bottom menu' ));
	}
	add_action( 'init', 'wpb_custom_new_menu' );

	class Description_Walker extends Walker_Nav_Menu
	{
	    function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0)
	    {
	        $classes = empty($item->classes) ? array () : (array) $item->classes;
	        $class_names = join(' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) );
	        !empty ( $class_names ) and $class_names = ' class="'. esc_attr( $class_names ) . '"';
	        $output .= "";
	        $attributes  = '';
	        !empty( $item->attr_title ) and $attributes .= ' title="'  . esc_attr( $item->attr_title ) .'"';
	        !empty( $item->target ) and $attributes .= ' target="' . esc_attr( $item->target     ) .'"';
	        !empty( $item->xfn ) and $attributes .= ' rel="'    . esc_attr( $item->xfn        ) .'"';
	        !empty( $item->url ) and $attributes .= ' href="'   . esc_attr( $item->url        ) .'"';
	        $title = apply_filters( 'the_title', $item->title, $item->ID );
	        $item_output = $args->before
	        . "<a $attributes $class_names>"
	        . $args->link_before
	        . $title
	        . '</a>'
	        . $args->link_after
	        . $args->after;
	        $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
	    }
	}

	function header_nav() {
		wp_nav_menu(
		array(
			'theme_location'  => '',
			'menu'            => '',
			'container'       => '',
			'container_class' => '',
			'container_id'    => '',
			'menu_class'      => '',
			'menu_id'         => '',
			'echo'            => true,
			'fallback_cb'     => 'wp_page_menu',
			'before'          => '',
			'after'           => '',
			'link_before'     => '',
			'link_after'      => '',
            'items_wrap'      => '%3$s',		
            'depth'           => 0,
			'walker'          => new Description_Walker
			)
		);
	}

	// ACF - Options Page 
	if( function_exists('acf_add_options_page') ) {
		
		acf_add_options_page(array(
			'page_title' 	=> 'Home page settings',
		 	'menu_title'	=> 'Home page settings',
			'menu_slug' 	=> 'theme-general-settings',
			'capability'	=> 'edit_posts',
			'redirect'		=> false
		));
		
	}

	// FILTER
	function sortIt($sortType)
	{
	    global $wp_query;

	    if (strcmp($sortType, 'taxi') == 0 )
	    {
	        $newQuery = new WP_Query( 
	        	array( 
	        		'orderby' => 'title',
	        		'order'=> 'DESC',
	        		'cat' => 7,
	        		'posts_per_page' => '-1',
	        		'post_status'      => 'publish'
	        	) 
	        );
	    }

	    if (strcmp($sortType, 'dzielone') == 0 )
	    {
	        $newQuery = new WP_Query( 
	        	array( 
	        		'orderby' => 'title', 
	        		'order'=> 'DESC', 
	        		'cat' => 10, 
	        		'posts_per_page' => '-1',
	        		'post_status'      => 'publish'
	        	) 
	        );
	    }

	    if (strcmp($sortType, 'mocowania') == 0 )
	    {
	        $newQuery = new WP_Query( 
	        	array( 
	        		'orderby' => 'title' , 
	        		'order'=> 'DESC', 
	        		'cat' => 11, 
	        		'posts_per_page' => '-1',
	        		'post_status'      => 'publish'
	        	) 
	        );
	    }

	    return $newQuery;
	}

	// Remove WP EMOJI
	remove_action('wp_head', 'print_emoji_detection_script', 7);
	remove_action('wp_print_styles', 'print_emoji_styles');

	remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
	remove_action( 'admin_print_styles', 'print_emoji_styles' );

	// Show thumbnails
	add_theme_support('post-thumbnails'); 
	add_image_size('thumbnail-size', 580, 480, true);
	if (has_post_thumbnail()) {
	    $thumbnail_data = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'thumbnail-size' );
	    $thumbnail_url = $thumbnail_data[0];
	}


	// Page templates
	add_filter( 'single_template',
    create_function( '$t', 'foreach( (array) get_the_category() as $cat ) {
        if ( file_exists(TEMPLATEPATH . "/single-{$cat->slug}.php") ) return TEMPLATEPATH . "/single-{$cat->slug}.php";
    } return $t;' ) );

    // Delete admin bar (margin-top: 32px) when logged in
	add_filter('show_admin_bar', '__return_false');

?>