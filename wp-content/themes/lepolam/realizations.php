<div class="slider">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 text-box">
				<div class="row">
					<h2 class="title">Nasze realizacje</h2>
					<a class="btn btn-default" href="/projekty">Zobacz wszystkie realizacje</a>
				</div>
			</div>	
		</div>
	</div>
	<div id="realizations_slider" class="owl-carousel">
		<?php 
			$args = array(
				'posts_per_page' => -1,
				'offset'           => 0,
				'orderby'          => 'date',
				'order'            => 'DESC',
				'post_type'        => 'post',
				'post_status'      => 'publish',
				'suppress_filters' => true 
			);
			$posts = get_posts( $args ); 
		?>
		<?php foreach ( $posts as $post ) : setup_postdata( $post ); ?>
			<a href="<?php the_permalink(); ?>"><img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id() );?>" alt=""></a>
		<?php endforeach; 
		wp_reset_postdata();
		?>
	</div>
</div>
