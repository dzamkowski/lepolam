jQuery(document).ready(function($) {

// Google Maps
  $(window).load(function(){
    function initMap() {
      var uluru = {lat: 50.81938199999999, lng: 20.539023000000043};
      var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 15,
        center: uluru
      });
      var marker = new google.maps.Marker({
        position: uluru,
        map: map,
        icon: 'http://lepolam.test.designum.pl/wp-content/uploads/2018/04/marker.png'
      });
    };

    initMap();
  }); 

// Sliders - owl carousel
  $('#banner_slider').owlCarousel({
      mouseDrag: false,
      autoplay: true,
      animateIn: 'fadeIn',
      animateOut: 'fadeOut',
      loop: true,
      items: 1,
      margin: 0,
      dots: false,
      smartSpeed: 700
  });

  $('#realizations_slider').owlCarousel({
      autoplay: true,
      loop: true,
      items: 6,
      margin: 0,
      dots: false,
      smartSpeed: 800,
      responsive: {

      0: {
        items: 1,
      },

      380: {
        items: 2,
      },

      768: {
        items: 3,
      }, 

      992: {
        items: 4,
      },

      1200: {
        items: 6
      }  

    }
  });

// Menu
  $('.mobile-menu-icon').click(function(){
    $(this).toggleClass('open');
    $('.mobile-menu').toggleClass('open');
    $('.menu-box').toggleClass('open');
  });

// Scroll
  $(".to-products").click(function() {
      $('html, body').animate({
          scrollTop: $("#products").offset().top
      }, 700);
  });
  
});

