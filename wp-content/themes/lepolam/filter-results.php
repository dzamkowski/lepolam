<?php
	if (array_key_exists("sortby", $_GET) === false) { ?>

	<?php if ( is_page('lampy-taxi')) { ?>
		<?php 
			$args = array(
				'posts_per_page' => -1,
				'offset'           => 0,
				'category'         => '5',
				'orderby'          => 'date',
				'order'            => 'DESC',
				'post_type'        => 'post',
				'post_status'      => 'publish',
				'suppress_filters' => true 
			);
			$posts = get_posts( $args ); 
		?>
		<?php foreach ( $posts as $post ) : setup_postdata( $post ); ?>
			<a href="<?php the_permalink(); ?>" class="product col-xs-3 col-md-3">
				<img class="product-img" src="<?php echo wp_get_attachment_url( get_post_thumbnail_id() );?>" alt="">
				<p class="product-title"><?php the_title(); ?></p>
			</a>
		<?php endforeach; ?>
	<?php }; ?>

	<?php if ( is_page('produkty-marki-esiva')) { ?>
		<?php 
			$args = array(
				'posts_per_page' => -1,
				'offset'           => 0,
				'category'         => '8',
				'orderby'          => 'date',
				'order'            => 'DESC',
				'post_type'        => 'post',
				'post_status'      => 'publish',
				'suppress_filters' => true 
			);
			$posts = get_posts( $args ); 
		?>
		<?php foreach ( $posts as $post ) : setup_postdata( $post ); ?>
			<a href="<?php the_permalink(); ?>" class="product col-xs-3 col-md-3">
				<img class="product-img" src="<?php echo wp_get_attachment_url( get_post_thumbnail_id() );?>" alt="">
				<p class="product-title"><?php the_title(); ?></p>
			</a>
		<?php endforeach; ?>
	<?php }; ?>

	<?php if ( is_page('lampy-reklamowe')) { ?>
		<?php 
			$args = array(
				'posts_per_page' => -1,
				'offset'           => 0,
				'category'         => '6',
				'orderby'          => 'date',
				'order'            => 'DESC',
				'post_type'        => 'post',
				'post_status'      => 'publish',
				'suppress_filters' => true 
			);
			$posts = get_posts( $args ); 
		?>
		<?php foreach ( $posts as $post ) : setup_postdata( $post ); ?>
			<a href="<?php the_permalink(); ?>" class="product col-xs-3 col-md-3">
				<img class="product-img" src="<?php echo wp_get_attachment_url( get_post_thumbnail_id() );?>" alt="">
				<p class="product-title"><?php the_title(); ?></p>
			</a>
		<?php endforeach; ?>
	<?php }; ?>

<?php }; ?>


<?php if (array_key_exists("sortby", $_GET) === true) { ?>
	
	<?php $newQuery = sortIt($_GET['sortby']); ?>
	
	<?php if ( $newQuery->have_posts() ) : while ( $newQuery->have_posts() ) : $newQuery->the_post(); ?>
		<a href="<?php the_permalink(); ?>" class="product col-xs-3 col-md-3">
			<img class="product-img" src="<?php echo wp_get_attachment_url( get_post_thumbnail_id() );?>" alt="">
			<p class="product-title"><?php the_title(); ?></p>
		</a>
	<?php endwhile; ?>
<?php wp_reset_postdata(); ?>

<?php endif; ?>
<?php }; ?>