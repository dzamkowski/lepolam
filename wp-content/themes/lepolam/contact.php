<?php /* Template Name: Contact */ ?>

<!DOCTYPE html>
<html>
	<head>
	    <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Lepolam - produkcja na najwyższym poziomie</title>
		<?php wp_head(); ?>
	</head>
	<body>
	<header class="header page-header">
	<div class="container">
		<div class="row">
			<?php get_template_part('menu'); ?>
		</div>
		</div>
			<div class="banner-page">
				<div class="container">
					<div class="row">
						<div class="col-xs-12">
							<div class="row">
								<div class="title-page">
									<?php the_title(); ?>
								</div>
								<img class="cert" src="<?php echo get_bloginfo('template_url') ?>/images/cert-logo.png" alt="Certyfikat Produkt Polski">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>
		<main>
			<div class="single container">
				<div class="row">
					<div class="top">
						<div id="map"></div>
						<div class="col-xs-12 col-sm-6 contact-details">
							<div class="row">
								<h2 class="title">Lepolam Wichrowscy sp.j</h2>
								<p>ul. Przemysłowa 35</p>
								<p>26-052 Nowiny k/Kielc</p>
								<br>
								<p><i class="fa fa-phone"></i>41 348 03 63</p> 
								<p><i class="fa fa-envelope"></i>sprzedaz@lepolam.com.pl</p>
								<br>
								<p>NIP: 657-181-59-22</p>
								<p>REGON: 290455069</p>
							</div>
						</div>
						<div class="col-xs-12 col-sm-6">
							<div class="row">
								<?php echo do_shortcode( ' [contact-form-7 id="221" title="Formularz kontaktowy"] ' ); ?>
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-4 contact-details">
						<div class="row">
							<h2 class="title">Zarząd</h2>
							<p><strong>Antoni Wichrowski</strong></p>
							<p>Telefon: 607 217 812</p>
							<br>
							<p><strong>Maria Wichrowska</strong></p>
							<p>Telefon: 604 172 810</p>
							<p>E-mail: mwichrowska@lepolam.com.pl</p>
							<br>
							<p><strong>Jarosław Wichrowski </strong></p>
							<p>Telefon: 602 272 979</p>
							<p>E-mail: jkwichrowski@lepolam.com.pl </p>
						</div>
					</div>
					<div class="col-xs-12 col-sm-4 contact-details">
						<div class="row">
							<h2 class="title">Dział Sprzedaży:</h2>
							<p class="subtitle"><strong>Lampy TAXI:</strong></p>
							<p><strong>Barbara Kałczuga</strong></p>
							<p>Telefon: tel. 41-348-03-63 | 41-348-03-64</p>
							<p>Kom. 695-613-842</p>
							<p>E-mail: sprzedaz@lepolam.com.pl</p>
							<br>
							<p><strong>Renata Niepłoch</strong></p>
							<p>Telefon: 41-348-03-63 | 41-348-03-64</p>
							<p>Kom. 668-811-508</p>
							<p>E-mail: sprzedaz@lepolam.com.pl</p>
							<p>E-mail: platnosci@lepolam.com.pl</p>
						</div>
					</div>
					<div class="col-xs-12 col-sm-4 contact-details">
						<div class="row">
							<h2 class="title">Dział Sprzedaży:</h2>
							<p class="subtitle"><strong>Lampy Reklamowe, Usługi Termoformowania i Frezowania:</strong></p>
							<p><strong>Jarosław Wichrowski</strong> - właściciel</p>
							<p>Kom. 602-272-979 </p>
							<p>E-mail: jkwichrowski@lepolam.com.pl</p>
							<br>
							<a class="subtitle" href="http://lepolam.test.designum.pl/serwis/"><strong>Serwis fabryczny taksometrów i kas fiskalnych alwi elektronik</strong></a>
							<p><strong>Mariusz Bąk</strong> - kierownik serwisu</p>
							<p>Kom. 695-613-843</p>
							<p>E-mail: serwis@alwi.com.pl</p>
						</div>
					</div>
				</div>
			</div>
			<?php get_template_part('realizations'); ?> 
		</main>
	<?php get_footer(); ?>
	</body>
</html>