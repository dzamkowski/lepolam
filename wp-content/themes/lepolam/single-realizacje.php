<!DOCTYPE html>
<html>
	<head>
	    <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <link href="<?php echo get_template_directory_uri(); ?>/images/favicon.ico" rel="shortcut icon" />
		<title><?php wp_title(); ?></title>
		<?php wp_head(); ?>
	</head>
	<body>
	<header class="header page-header">
	<div class="container">
		<div class="row">
			<?php get_template_part('menu'); ?>
		</div>
		</div>
			<div class="banner-page">
				<div class="container">
					<div class="row">
						<div class="col-xs-12">
							<div class="row">
								<h1 class="title-page">
									<?php
										foreach((get_the_category()) as $category) { 
										    echo $category->cat_name . ' '; 
										} 
									?>
								</h1>
								<img class="cert" src="<?php echo get_bloginfo('template_url') ?>/images/cert-logo.png" alt="Certyfikat Produkt Polski">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>
		<main>
			<div class="single container">
				<div class="row">
					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					<p class="title"><?php the_title(); ?></p>
					<div class="content"><?php the_content(); ?></div>
					<div class="gallery">
						<p class="title gallery-title"><?php echo get_field('gallery_title'); ?></p>
						<?php while( have_rows('realization_image') ): the_row(); ?>
						<div class="gallery-item col-xs-6 col-md-4 col-lg-4">
							<div class="row">
								<a href="<?php the_sub_field('image'); ?>" data-lightbox="realizations">
									<img src="<?php the_sub_field('image'); ?>" alt="">
								</a>
							</div>
						</div>
						<?php endwhile; ?>

					</div>

					<?php endwhile; ?>
					<?php endif; ?>
				</div>
			</div>
			<?php get_template_part('realizations'); ?>
		</main>
	<?php get_footer(); ?>
	</body>
</html>