<?php /* Template Name: Product */ ?>

<!DOCTYPE html>
<html>
	<head>
	    <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <link href="<?php echo get_template_directory_uri(); ?>/images/favicon.ico" rel="shortcut icon" />
		<title><?php wp_title(); ?></title>
		<?php wp_head(); ?>
	</head>
	<body>
	<?php get_template_part('page-header'); ?>
		<main>
			<div class="product-page container">
				<h1 class="col-xs-12 page-title"><?php wp_title(''); ?></h1>
				<div class="product-photos row">
					<?php while( have_rows('product_image') ): the_row(); ?>
					<div class="product-img col-xs-6 col-md-6">
						<img src="<?php the_sub_field('image'); ?>" alt="">
					</div>
					<?php endwhile; ?>
				</div>
				<div class="product-specs row">
					<?php while( have_rows('product_text') ): the_row(); ?>
					<div class="text-box col-xs-12 col-md-6 col-lg-4">
						<div class="row">
							<p class="title"><?php the_sub_field('title'); ?></p>
							<div class="desc"><?php the_sub_field('text'); ?></div>
						</div>
					</div>
					<?php endwhile; ?>
				</div>
				<div class="product-price row">
					<div class="price-box col-xs-12 col-md-6">
						<p class="title">Cennik</p>
						<?php while( have_rows('product_pricing') ): the_row(); ?>
						<div class="single-price col-xs-12 col-md-6">
							<p class="sub-title"><?php the_sub_field('title'); ?></p>
							<div class="desc"><?php the_sub_field('text'); ?></div>
						</div>
						<?php endwhile; ?>
					</div>
					<div class="accessory-box col-xs-12 col-md-6">
						<p class="title">Akcesoria</p>
						<?php while( have_rows('product_accessories') ): the_row(); ?>
						<div class="single-accessory col-xs-12 col-md-6">
							<p class="sub-title"><?php the_sub_field('title'); ?></p>
							<div class="desc"><?php the_sub_field('text'); ?></div>
						</div>
						<?php endwhile; ?>
					</div>
				</div>
			</div>
			<?php get_template_part('realizations'); ?>
		</main>
	<?php get_footer(); ?>
	</body>
</html>