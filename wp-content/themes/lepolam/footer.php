<script>function WHCreateCookie(name,value,days){var date=new Date();date.setTime(date.getTime()+(days*24*60*60*1000));var expires="; expires="+date.toGMTString();document.cookie=name+"="+value+expires+"; path=/"}function WHReadCookie(name){var nameEQ=name+"=";var ca=document.cookie.split(';');for(var i=0;i<ca.length;i++){var c=ca[i];while(c.charAt(0)==' ')c=c.substring(1,c.length);if(c.indexOf(nameEQ)==0)return c.substring(nameEQ.length,c.length)}return null}window.onload=WHCheckCookies;function WHCheckCookies(){if(WHReadCookie('cookies_accepted')!='T'){var message_container=document.createElement('div');message_container.id='cookies-message-container';var html_code='<div id="cookies-message"><p>Ta strona używa plików cookies. Pozostając na tej stronie, wyrażasz zgodę na korzystanie z plików cookies. Więcej informacji można znaleźć w <a href="<?php echo get_bloginfo('template_url') ?>/images/cookies_regulamin.pdf" target="_blank">Polityce w sprawie Cookies.</a><a href="javascript:WHCloseCookiesWindow();" id="accept-cookies-checkbox" name="accept-cookies"><span><i class="fa fa-times" aria-hidden="true"></i></span></a></p></div>';message_container.innerHTML=html_code;document.body.appendChild(message_container)}}function WHCloseCookiesWindow(){WHCreateCookie('cookies_accepted','T',365);document.getElementById('cookies-message-container').removeChild(document.getElementById('cookies-message'))}
</script>

<footer class="footer container-fluid">
	<div class="row top">
		<div class="col-xs-12 text-center">
			<div class="phone-box">
				<i class="fa fa-phone" aria-hidden="true"></i>
				<a class="phone" href="tel:413480363">41 348 03 63</a>
			</div>
			<div class="email-box">
				<i class="fa fa-envelope-o" aria-hidden="true"></i>
				<a class="email" href="mailto:sprzedaz@lepolam.com.pl">sprzedaz@lepolam.com.pl</a>
			</div>
		</div>
		<div class="col-xs-12 text-center address-box">
			<i class="fa fa-map-marker" aria-hidden="true"></i>
			<p class="address">Lepolam Wichrowscy sp.j, ul. Przemysłowa 35, 26-052 Nowiny k/Kielc</p>
		</div>
	</div>
	<div class="row bottom text-center">
		<div class="col-xs-12">
			<p class="copy">Copyright 2018 © Lepolam</p>
			<p class="design">Designed by <a href="http://designum.pl/" target="_blank">Designum.pl</a></p>
		</div>
		<div class="col-xs-12">
			<img src="<?php echo get_bloginfo('template_url') ?>/images/footer-img.png" alt="Lepolam">
			<div class="text container">
				<p>Szanowni Państwo!</p><br>
				<p>Jest nam niezmiernie miło podzielić się z Państwem informacją, że nasza firma jako jedna z dwóch w województwie Świętokrzyskim otrzymała dotację na realizację projektu badawczo – rozwojowego w ramach <strong>Programu Operacyjnego Innowacyjny Rozwój 2014-2020.</strong> <a href="http://lepolam.test.designum.pl/ue">Zachęcamy do zapoznania się ze szczegółami.</a></p><br>
				<p>Z wyrazami szacunku,</p>
				<p>Zarząd Spółki</p>
			</div>
		</div>
	</div>
</footer>