<?php /* Template Name: Products */ ?>

<!DOCTYPE html>
<html>
	<head>
	    <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <link href="<?php echo get_template_directory_uri(); ?>/images/favicon.ico" rel="shortcut icon" />
		<title><?php wp_title(); ?></title>
		<?php wp_head(); ?>
	</head>
	<body>
	<?php get_template_part('page-header'); ?>
		<main>
			<div class="products-page container">
				<div class="row">
					<div class="products-filter col-xs-12">
						<a class="btn" href="?sortby=taxi">Lampy taxi</a>
						<a class="btn" href="?sortby=dzielone">Lampy dzielone</a>
						<a class="btn" href="?sortby=mocowania">Mocowania lamp</a>
					</div>
					<div id="filter_results">
						<?php get_template_part('filter-results'); ?>
					</div>
				</div>
			</div>
			<?php get_template_part('realizations'); ?>
		</main>
	<?php get_footer(); ?>
	</body>
</html>