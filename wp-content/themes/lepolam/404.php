<!DOCTYPE html>
<html>
	<head>
	    <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Lepolam - produkcja na najwyższym poziomie</title>
		<?php wp_head(); ?>
	</head>
	<body>
	<?php get_template_part('page-header'); ?>
		<main>
			<div class="products-page container">
				<div class="row error">
					<p class="title">Strona, na którą pragniesz przejść nie istnieje!</p>
					<div class="title"><a href="/">Powróć na stronę główną</a></div>
				</div>
			</div>
			<?php get_template_part('realizations'); ?>
		</main>
	<?php get_footer(); ?>
	</body>
</html>