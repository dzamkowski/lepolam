<!DOCTYPE html>
<html>
	<head>
	    <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <link href="<?php echo get_template_directory_uri(); ?>/images/favicon.ico" rel="shortcut icon" />
		<title><?php wp_title(); ?></title>
		<?php wp_head(); ?>
	</head>
	<body>
	<?php get_header(); ?>
		<main>
			<?php get_template_part('aside'); ?> 
			<?php get_template_part('products'); ?> 
			<?php get_template_part('news'); ?>    
			<?php get_template_part('realizations'); ?>
		</main>
	<?php get_footer(); ?>
	</body>
</html>